FROM alpine:latest

RUN apk --update upgrade
RUN apk add php php-tokenizer php-xml php-xmlwriter php-simplexml nodejs npm composer

RUN composer global require squizlabs/php_codesniffer
RUN ln -s /root/.composer/vendor/bin/phpcs /usr/local/bin/phpcs
RUN npm i -g stylelint eslint

RUN mkdir /app
WORKDIR /app
